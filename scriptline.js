var tts = window.speechSynthesis;
var voices = [];
var rateIndicator = document.getElementById('rateIndicator');
var rateInput = document.getElementById('rateInput');
var pitchIndicator = document.getElementById('pitchIndicator');
var pitchInput = document.getElementById('pitchInput');
rateInput.addEventListener('change', function () {
	rateIndicator.textContent = rateInput.value
})


pitchInput.addEventListener('change', function () {
	pitchIndicator.textContent = pitchInput.value
})
GetVoices();
// to make sure the speechsyntesis is exist on chrome
if (speechSynthesis !== undefined) {
	speechSynthesis.onvoiceschanged = GetVoices;
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}


function speakMessage(message, PAUSE_MS = 500) {
	try {
		const messageParts = message.split(',')

		let currentIndex = 0
		const speak = (textToSpeak) => {
			const msg = new SpeechSynthesisUtterance();
			//   const voices = window.speechSynthesis.getVoices();
			const voices = tts.getVoices();;

			const voiceList = document.querySelector('#voiceList');
			msg.voice = voices[11];
			msg.volume = 1;
			msg.rate = 1;

			msg.text = textToSpeak;


			msg.onend = function () {
				currentIndex++;
				if (currentIndex < messageParts.length) {
					setTimeout(() => {
						speak(messageParts[currentIndex])
					}, PAUSE_MS)
				}
			};
			speechSynthesis.speak(msg);
		}
		speak(messageParts[0])
	} catch (e) {
		console.error(e)
	}
}


function speak(pause) {
var txtPanggilan = document.querySelector('#panggilan');
	var txtSection = document.querySelector('#section');
	var txtLine = document.querySelector('#line');
	var txtLot = document.querySelector('#lot');
	var txtLantai = document.querySelector('#lantai');
	speakMessage(" MOHON PERHATIAN !, PANGGILAN KEPADA : , " + txtPanggilan.value + " , SECTION : , " + txtSection.value + " , DIMOHON SEGERA MENUJU LINE : , " + txtLine.value + " , LOT :  " + txtLot.value + " , LANTAI : , " + txtLantai.value + " , SEKALI LAGI", pause);
		sleep(25000).then(() => {
			speakMessage(" MOHON PERHATIAN !, PANGGILAN KEPADA : , " + txtPanggilan.value + " , SECTION : , " + txtSection.value + " , DIMOHON SEGERA MENUJU LINE : , " + txtLine.value + " , LOT :  " + txtLot.value + " , LANTAI : , " + txtLantai.value + " , TERIMA KASIH", pause);
		});

}

function GetVoices() {
	voices = tts.getVoices();
	voiceList.innerHTML = '';
	voices.forEach((voice) => {
		var listItem = document.createElement('option');
		listItem.textContent = voice.name;
		listItem.setAttribute('data-lang', voice.lang);
		listItem.setAttribute('data-name', voice.name);
		voiceList.appendChild(listItem);
	});

	voiceList.selectedIndex = 14;
}


